from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin

from receipts.models import Account, ExpenseCategory, Receipt

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

    def get_context_data(self, **kwargs):
    # Let the parent class get the context for
    # the actual Post for its detail
        context = super().get_context_data(**kwargs)

    # Print it out to see what's in it
        from pprint import pprint
        pprint(context)

    # Return it like nothing ever happened
        return context


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    template_name = "receipts/create.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    fields = ["name", "number"]
    template_name = "accounts/create.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.author = self.request.user
        item.save()
        return redirect("home")


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(author=self.request.user)

    def get_context_data(self, **kwargs):
    # Let the parent class get the context for
    # the actual Post for its detail
        context = super().get_context_data(**kwargs)

    # Print it out to see what's in it
        from pprint import pprint
        pprint(context)

    # Return it like nothing ever happened
        return context


class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    fields = ["name"]
    template_name = "categories/create.html"

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("home")


class CategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "categories/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)
