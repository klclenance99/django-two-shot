from django.contrib import admin
from receipts.models import ExpenseCategory
from receipts.models import Account
from receipts.models import Receipt

# Register your models here.
class ReceiptAdmin(admin.ModelAdmin):
    pass


admin.site.register(Receipt, ReceiptAdmin)

class AccountAdmin(admin.ModelAdmin):
    pass


admin.site.register(Account, AccountAdmin)


class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(ExpenseCategory, ExpenseCategoryAdmin)